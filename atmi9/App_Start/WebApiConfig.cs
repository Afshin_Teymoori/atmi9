﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace atmi9
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

			// Todo : XmlFormatter has been removed entirly. Better approach would be to repriotrize the Content Negotioation to return Json first and others if json fails 
			config.Formatters.XmlFormatter.SupportedMediaTypes.Clear();

			//config.Routes.MapHttpRoute(
			//	name: "DefaultApi",
			//	routeTemplate: "api/{controller}/{id}",
			//	defaults: new { id = RouteParameter.Optional }
			//);
        }
    }
}
