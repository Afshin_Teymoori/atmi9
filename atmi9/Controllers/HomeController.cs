﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using Newtonsoft.Json.Linq;

namespace atmi9.Controllers
{

	//[RoutePrefix("api/V1")]
	[EnableCors("*", "*", "*")]
	public class HomeController : ApiController
	{
		[Route("")]
		[HttpGet]
		public async Task<IHttpActionResult> Get()
		{
			var message = string.Empty;
			var result = await Task.Run(() => "the service does not accept get request");
			return BadRequest(result);
		}

		[Route("{input}")]
		[HttpPost]
		public async Task<IHttpActionResult> ReturnDatafromQueryString(string input)
		{
			dynamic payload = JObject.Parse(input);

			var message = string.Empty;
			var result = await Task.Run(() => "We got the data.");

			return Ok(result);
		}

		[Route("")]
		[HttpPost]
		public async Task<IHttpActionResult> ReturnDatafromBody(object input)
		{
			JObject inputData = null;

			var validlist = new List<ExpandoObject>();
			try
			{
				inputData = JObject.Parse(input.ToString());
				JToken payload = null;

				if (inputData.TryGetValue("payload", out payload))
				{

					foreach (var item in payload)
					{
						if (IsItemValid(item))
						{
							validlist.Add(JTokenToResponse(item));
						}
					}
				}
				else
				{
					throw new ApplicationException("no payload key found");
				}


				var result = await Task.Run(() =>
				{
					dynamic expando = new ExpandoObject();
					expando.response = validlist;
					return expando;
				});

				return Ok(result);
			}
			catch (Exception ex)
			{
				dynamic ErrorMessage = new ExpandoObject();
				ErrorMessage.error = "Could not decode request: JSON parsing failed" ;
				//var errorResponse = new ErrorResponse() {error = "Could not decode request: JSON parsing failed"};

				//return BadRequest("Could not decode request: JSON parsing failed");
				return Content(HttpStatusCode.BadRequest, ErrorMessage);
			}
		}

		private bool IsItemValid(JToken jtoken)
		{
			if (!jtoken.HasValues) return false;
			var drm = jtoken.Value<bool?>("drm") ?? false;
			var episodeCount = jtoken.Value<int?>("episodeCount") ?? 0;
			return (drm && episodeCount > 0);
		}

		private ExpandoObject JTokenToResponse(JToken item)
		{
			if (item == null) return null;

			dynamic expando = new ExpandoObject();
			var imageObject = item.Value<JToken>("image");
			if (imageObject.HasValues)
			{
				expando.image = imageObject.Value<string>("showImage");
			}
			expando.slug = item.Value<string>("slug");
			expando.title = item.Value<string>("title");
			return expando;

		}

	}
}
