
### What is this repository for? ###

* This repository includes a .NET WebApi 2 project to which  you can post a json  array and it will filter the json array based on the following criteria 

It returns the ones with DRM enabled (drm: true) and at least one episode (episodeCount > 0)

you can see [Sample request json](http://mi9-coding-challenge.herokuapp.com/sample_request.json) and [sample response json](http://mi9-coding-challenge.herokuapp.com/sample_response.json)

### How do I get set up? ###

* Clone the repo to your local machine 
* go to the folder atmi9 
* Open solution atmi9.sln in visual studio 2013 +
* and build the project
* How to run tests
* Deployment instructions

### How Do I test it ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact